<?php
require_once('Voiture.php');

class VoitureStorageDB implements VoitureStorage
{

    private $bd;

    public function __construct($bd)
    {
        $this->bd = $bd;
    }

    public function create(Voiture $voiture)
    {
        $sql = "INSERT INTO voitures (categorie, marque, modele, annee, image) VALUES (?,?,?,?,?)";
        $this->bd->prepare($sql)->execute([$voiture->getCategorie(), $voiture->getMarque(), $voiture->getModele(), $voiture->getAnnee(), $voiture->getImage()]);
    }

    public function update($id, Voiture $voiture)
    {
        $sql = "UPDATE voitures SET categorie=?, marque=?, modele=?, annee=?, image=? WHERE id=?";
        $result = $this->bd->prepare($sql);
        $result->execute([$voiture->getCategorie(), $voiture->getMarque(), $voiture->getModele(), $voiture->getAnnee(), $voiture->getImage(), $id]);
    }

    public function delete($id)
    {
        $sql = "DELETE FROM voitures WHERE id=?";
        $result = $this->bd->prepare($sql);
        $result->execute([$id]);
    }

    public function deleteAll()
    {
    }

    public function read($id)
    {
        $sql = "SELECT * FROM voitures WHERE id=" . $id;
        $req = $this->bd->prepare($sql);
        $req->execute();
        $row = $req->fetch();
        return new Voiture($row['id'], $row['categorie'], $row['marque'], $row['modele'], $row['annee'], $row['image']);
    }

    public function readAll()
    {
        $sql = "SELECT * FROM voitures";
        $req = $this->bd->prepare($sql);
        $req->execute();
        $result = $req->fetchAll(\PDO::FETCH_ASSOC);
        $voitures = array();
        for ($i = 0; $i < count($result); $i++) {
            $voiture = new Voiture($result[$i]['id'], $result[$i]['categorie'], $result[$i]['marque'], $result[$i]['modele'], $result[$i]['annee'], $result[$i]['image']);
            array_push($voitures, $voiture);
        }
        return $voitures;
    }
}

?>
