<?php

require_once("Voiture.php");

interface VoitureStorage
{
    public function create(Voiture $voiture);

    public function update($id, Voiture $voiture);

    public function delete($id);

    public function deleteAll();

    public function read($id);

    public function readAll();

}

?>
