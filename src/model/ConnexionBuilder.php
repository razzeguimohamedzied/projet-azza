<?php

class ConnexionBuilder
{
    protected $data;
    protected $errors;

    public function __construct($data = null)
    {
        if ($data === null) {
            $data = array(
                "email" => "",
                "password" => "",
            );
        }
        $this->data = $data;
        $this->errors = array();
    }

    public function getEmailRef()
    {
        return array(
            "name" => "email",
            "type" => "email",
            "placeholder" => "Email",
        );
    }

    public function getPasswordRef()
    {
        return array(
            "name" => "password",
            "type" => "password",
            "placeholder" => "Mot De Passe",
        );
    }
}

?>
