<?php

class Voiture
{
    protected $id;
    protected $categorie;
    protected $marque;
    protected $modele;
    protected $annee;
    protected $image;

    public function __construct($id, $categorie, $marque, $modele, $annee, $image)
    {
        $this->id = $id;
        $this->categorie = $categorie;
        $this->marque = $marque;
        $this->modele = $modele;
        $this->annee = $annee;
        $this->image = $image;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCategorie()
    {
        return $this->categorie;
    }

    public function getMarque()
    {
        return $this->marque;
    }

    public function getModele()
    {
        return $this->modele;
    }

    public function getAnnee()
    {
        return $this->annee;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }


}

?>
