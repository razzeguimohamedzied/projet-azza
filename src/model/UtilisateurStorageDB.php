<?php

require_once('Utilisateur.php');

class UtilisateurStorageDB implements UtilisateurStorage
{

    private $bd;

    public function __construct($bd)
    {
        $this->bd = $bd;
    }

    public function create(Utilisateur $utilisateur)
    {
        $sql = "INSERT INTO utilisateurs (nom, prenom, email, password) VALUES (?,?,?,?)";
        $this->bd->prepare($sql)->execute([$utilisateur->getNom(), $utilisateur->getPrenom(), $utilisateur->getEmail()
            , password_hash($utilisateur->getPassword(), PASSWORD_BCRYPT)]);
    }
}

?>
