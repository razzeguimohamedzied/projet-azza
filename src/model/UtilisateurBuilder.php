<?php

require_once("model/Utilisateur.php");

class UtilisateurBuilder
{

    protected $data;
    protected $errors;

    public function __construct($data = null)
    {
        if ($data === null) {
            $data = array(
                "nom" => "",
                "prenom" => "",
                "email" => "",
                "password" => "",
                "confirm_password" => "",
            );
        }
        $this->data = $data;
        $this->errors = array();
    }

    public function getNomRef()
    {
        return array(
            "name" => "nom",
            "type" => "text",
            "placeholder" => "Nom",
        );
    }

    public function getPrenomRef()
    {
        return array(
            "name" => "prenom",
            "type" => "text",
            "placeholder" => "Prenom",
        );
    }

    public function getEmailRef()
    {
        return array(
            "name" => "email",
            "type" => "email",
            "placeholder" => "Email",
        );
    }

    public function getPasswordRef()
    {
        return array(
            "name" => "password",
            "type" => "password",
            "placeholder" => "Mot De Passe",
        );
    }

    public function getConfirmPasswordRef()
    {
        return array(
            "name" => "confirm_password",
            "type" => "password",
            "placeholder" => "Confirmez Votre Mot De Passe",
        );
    }

    public function getData($ref)
    {
        return key_exists($ref, $this->data) ? $this->data[$ref] : '';
    }


    public function getErrors($ref)
    {
        return key_exists($ref, $this->errors) ? $this->errors[$ref] : '';
    }

    public function isValid()
    {
        $this->errors = array();

        if (!key_exists('nom', $this->data) || $this->data["nom"] === "") {
            $this->errors['nom'] = "Vous devez saisir votre nom";
        }
        if (!key_exists('prenom', $this->data) || $this->data["prenom"] === "") {
            $this->errors['prenom'] = "Vous devez saisir votre prenom";
        }
        if (!key_exists('email', $this->data) || $this->data["email"] === "") {
            $this->errors['email'] = "Vous devez saisir votre email";
        }
        if (!key_exists('password', $this->data) || $this->data["password"] === "") {
            $this->errors['password'] = "Vous devez saisir votre mot de passe";
        }

        if (!key_exists('confirm_password', $this->data) || $this->data["confirm_password"] === "") {
            $this->errors['confirm_password'] = "Vous devez confirmez votre mot de passe";
        } else if($this->data["confirm_password"] !==$this->data["password"] )
        {
            $this->errors['confirm_password'] = "Les 2 Mots de passes ne sont pas identiques ";

        }

        return count($this->errors) === 0;

    }

    public function createUtilisateur()
    {
        $utilisateur = new Utilisateur($this->data['nom'], $this->data['prenom'], $this->data['email'], $this->data['password']);
        return $utilisateur;
    }
}

?>
