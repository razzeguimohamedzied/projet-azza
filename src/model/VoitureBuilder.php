<?php
require_once("model/Voiture.php");

class VoitureBuilder
{
    protected $data;
    protected $errors;

    public function __construct($data = null)
    {
        if ($data === null) {
            $data = array(
                "categorie" => "",
                "marque" => "",
                "modele" => "",
                "annee" => ""
            );
        }
        $this->data = $data;
        $this->errors = array();
    }

    public function getCategorieRef()
    {
        return array(
            "name" => "categorie",
            "type" => "text",
            "placeholder" => "Catégorie",
        );
    }

    public function getMarqueRef()
    {
        return array(
            "name" => "marque",
            "type" => "text",
            "placeholder" => "Marque",
        );
    }

    public function getModeleRef()
    {
        return array(
            "name" => "modele",
            "type" => "text",
            "placeholder" => "Modéle",
        );
    }

    public function getAnneeRef()
    {
        return array(
            "name" => "annee",
            "type" => "text",
            "placeholder" => "Année",
        );
    }

    public function getFileRef()
    {
        return array(
            "name" => "file",
            "type" => "file"
        );
    }

    public function getData($ref)
    {
        return key_exists($ref, $this->data) ? $this->data[$ref] : '';
    }


    public function getErrors($ref)
    {
        return key_exists($ref, $this->errors) ? $this->errors[$ref] : '';
    }

    public function isValid()
    {
        $this->errors = array();

        if (!key_exists('categorie', $this->data) || $this->data["categorie"] === "") {
            $this->errors['categorie'] = "Vous devez saisir la catégorie";
        }

        if (!key_exists('marque', $this->data) || $this->data["marque"] === "") {
            $this->errors['marque'] = "Vous devez saisir la marque";
        }

        if (!key_exists('modele', $this->data) || $this->data["modele"] === "") {
            $this->errors['modele'] = "Vous devez saisir le modèle";
        }

        if (!key_exists('annee', $this->data) || $this->data["annee"] === "") {
            $this->errors['annee'] = "Vous devez saisir l'année";
        }
        return count($this->errors) === 0;
    }

    public function createVoiture()
    {
        $voiture = new Voiture(null,$this->data['categorie'], $this->data['marque'], $this->data['modele'], $this->data['annee'], null);
        return $voiture;
    }
}

?>
