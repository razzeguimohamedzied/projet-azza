<?php

require_once("Utilisateur.php");

interface UtilisateurStorage
{
    public function create(Utilisateur $utilisateur);
}

?>
