<?php

require_once("model/UtilisateurBuilder.php");
require_once("model/ConnexionBuilder.php");
require_once("model/VoitureBuilder.php");
require_once("Router.php");

class View
{

    protected $router;
    protected $content;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    function render()
    {
        include('template.php');
    }

    function showInscriptionPage(UtilisateurBuilder $builder)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Inscrivez-Vous</div>";
        $this->content .= "<form method='POST' action='" . $this->router->saveInscription() . "'>";
        $this->content .= self::getFormFields($builder);
        $this->content .= "<div>	<input class='inscription-button' type='submit' value='Inscription'/></div>";
        $this->content .= "</form>";
        $this->content .= "<a href='" . $this->router->showConnexion() . "'>Vous avez déja un compte ?</a>";
        $this->content .= "</div>";
    }

    protected function getFormFields(UtilisateurBuilder $builder)
    {
        $s = "";
        /*$s .= "<div><input class='input-contenue' name='". $nomRef['name']. "' type='text' placeholder='Nom'/></div>";*/

        $s .= self::tabBuilder($builder->getNomRef(), $builder);
        $s .= self::tabBuilder($builder->getPrenomRef(), $builder);
        $s .= self::tabBuilder($builder->getEmailRef(), $builder);
        $s .= self::tabBuilder($builder->getPasswordRef(), $builder);
        $s .= self::tabBuilder($builder->getConfirmPasswordRef(), $builder);

        return $s;
    }

    protected function tabBuilder($array, UtilisateurBuilder $builder)
    {
        $s = "<div class='container-input'><input class='input-contenue' ";
        foreach ($array as $key => $value) {
            $s .= $key . "='" . $value . "' ";
        }

        $s .= "value='" . $builder->getData($array['name']) . "'/>";


        $err = $builder->getErrors($array['name']);
        if ($err !== null) {
            $s .= '<div class="errror">' . $err . '</div>';
        }

        $s .= "</div>";
        return $s;
    }

    public function showConnexionPage(ConnexionBuilder $builder)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Connectez-Vous</div>";
        $this->content .= "<form method='POST'>";
        $this->content .= self::getFormConnexionFields($builder);
        $this->content .= "<div>	<input class='inscription-button' type='submit' value='Connexion'/></div>";
        $this->content .= "</form>";
        $this->content .= "<a href='" . $this->router->showInscriptionPage() . "'>Créer un compte</a>";
        $this->content .= "</div>";
    }

    protected function getFormConnexionFields(ConnexionBuilder $builder)
    {
        $s = "";
        $s .= self::tabBuilderConnexion($builder->getEmailRef());
        $s .= self::tabBuilderConnexion($builder->getPasswordRef());

        return $s;
    }

    protected function tabBuilderConnexion($array)
    {
        $s = "<div class='container-input'><input class='input-contenue' ";
        foreach ($array as $key => $value) {
            $s .= $key . "='" . $value . "' ";
        }
        $s .= "/></div>";
        return $s;
    }

    public function showFormVoiture(VoitureBuilder $builder)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Ajouter Une Voiture</div>";
        $this->content .= "<form method='POST' enctype='multipart/form-data' action='" . $this->router->saveVoiture() . "'>";
        $this->content .= self::getFormVoitureFields($builder);
        $this->content .= "<div>	<input class='inscription-button' type='submit' value='Ajouter'/></div>";
        $this->content .= "</form>";
        $this->content .= "</div>";
    }

    private function getFormVoitureFields(VoitureBuilder $builder)
    {
        $s = "";
        $s .= self::tabVoitureBuilder($builder->getCategorieRef(), $builder);
        $s .= self::tabVoitureBuilder($builder->getMarqueRef(), $builder);
        $s .= self::tabVoitureBuilder($builder->getModeleRef(), $builder);
        $s .= self::tabVoitureBuilder($builder->getAnneeRef(), $builder);

        $s .= "<input type='" . $builder->getFileRef()['type'] . "' name='" . $builder->getFileRef()['name'] . "'/>";
        return $s;
    }

    protected function tabVoitureBuilder($array, VoitureBuilder $builder)
    {
        $s = "<div class='container-input'><input class='input-contenue' ";
        foreach ($array as $key => $value) {
            $s .= $key . "='" . $value . "' ";
        }

        $s .= "value='" . $builder->getData($array['name']) . "'/>";


        $err = $builder->getErrors($array['name']);
        if ($err !== null) {
            $s .= '<div class="errror">' . $err . '</div>';
        }

        $s .= "</div>";
        return $s;
    }

    public function showListVoitures($voitures)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Liste Des Voitures</div>";
        $this->content .= "<div><div class='add-voiture'><a href='" . $this->router->formVoiture() . "'>Ajouter Une Voiture</a></div><table class='table-voiture'>";

        $this->content .= "<tr><td class='td-voiture-titre'>Catégorie</td><td class='td-voiture-titre'>Marque</td>
                            <td class='td-voiture-titre'>Modèle</td><td class='td-voiture-titre'>Année</td><td class='td-voiture-titre'>Photo</td><td class='td-voiture-titre'>Actions</td></tr>";
        for ($i = 0; $i < count($voitures); $i++) {
            $this->content .= "<tr>";
            $this->content .= "<td class='td-voiture'>" . $voitures[$i]->getCategorie() . "</td>";
            $this->content .= "<td class='td-voiture'>" . $voitures[$i]->getMarque() . "</td>";
            $this->content .= "<td class='td-voiture'>" . $voitures[$i]->getModele() . "</td>";
            $this->content .= "<td class='td-voiture'>" . $voitures[$i]->getAnnee() . "</td>";
            $this->content .= "<td class='td-voiture'>";
            if ($voitures[$i]->getImage() != null) {
                $this->content .= "<img width='70' src='" . $voitures[$i]->getImage() . "'/></td>";
            } else {
                $this->content .= "<img width='70' src='skin/images/defaut_image.gif'/>";
            }
            $this->content .= "</td>";
            /*<img width='80' src='" . ($voitures[$i]->getImage() === null ? 'skin/images/defaut_image.gif' : $voitures[$i]->getImage())
                . "'/></td>";*/
            $this->content .= "<td class='td-voiture'><a href='" . $this->router->showVoitureById($voitures[$i]->getId()) . "'>
                                                        <img src='skin/images/open-book.png' width='40'></a>
                                                     <a href='" . $this->router->updateVoitureById($voitures[$i]->getId()) . "'>
                                                     <img src='skin/images/edit.png' width='40'></a>
                                                     <a href='" . $this->router->deleteVoitureById($voitures[$i]->getId()) . "'>
                                                     <img src='skin/images/delete-forever--v1.png' width='40'></a></td>";
            $this->content .= "</tr>";
        }

        $this->content .= "</table></div>";
        $this->content .= "</div>";
    }

    public function getVoiture(Voiture $voiture)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Détails De La Voiture</div>";
        if ($voiture->getImage() != null) {
            $this->content .= "<div><img src='" . $voiture->getImage() . "' width='500'></div>";
        } else {
            $this->content .= "<div><img src='skin/images/defaut_image.gif' width='500'></div>";
        }
        $this->content .= "<div><span class='titre'>Catégorie : </span><span class='value'>" . $voiture->getCategorie() . "</span></div>";
        $this->content .= "<div><span class='titre'>Marque : </span><span class='value'>" . $voiture->getMarque() . "</span></div>";
        $this->content .= "<div><span class='titre'>Modèle : </span><span class='value'>" . $voiture->getModele() . "</span></div>";
        $this->content .= "<div><span class='titre'>Année : </span><span class='value'>" . $voiture->getAnnee() . "</span></div>";
        $this->content .= "</div>";

    }

    public function ajoutVoitureSuccess()
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Voiture ajoutée avec succès</div>";
        $this->content .= "<div><a href='" . $this->router->formVoiture() . "'>Ajouter Une Autre Voiture</a> </div>";
        $this->content .= "<div><a href='" . $this->router->listVoiture() . "'>Aller à la liste des voitures</a> </div>";
        $this->content .= "</div>";
    }

    public function showFormVoitureUpdate(VoitureBuilder $builder, $id)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Modifier Une Voiture</div>";
        $this->content .= "<form method='POST' enctype='multipart/form-data' action='" . $this->router->updateVoiture($id) . "'>";
        $this->content .= self::getFormVoitureFields($builder);
        $this->content .= "<div>	<input class='inscription-button' type='submit' value='Modifier'/></div>";
        $this->content .= "</form>";
        $this->content .= "</div>";
    }

    public function updateVoitureSuccess()
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Voiture modifiée avec succès</div>";
        $this->content .= "<div><a href='" . $this->router->listVoiture() . "'>Aller à la liste des voitures</a> </div>";
        $this->content .= "</div>";
    }

    public function deleteVoiture($id)
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Voulez Vous Vraiment Supprimez La Voiture</div>";
        $this->content .= "<div><a href='" . $this->router->listVoiture() . "'>Non</a> </div>";
        $this->content .= "<div><a href='" . $this->router->confirmerSuppression($id) . "'>Oui</a> </div>";
        $this->content .= "</div>";
    }

    public function deleteVoitureSuccess()
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Voiture supprimée avec succès</div>";
        $this->content .= "<div><a href='" . $this->router->listVoiture() . "'>Aller à la liste des voitures</a> </div>";
        $this->content .= "</div>";
    }

    public function inscriptionSuccess()
    {
        $this->content = "";
        $this->content .= "<div class='contenue'><div class='inscription-title'>Votre compte est crée avec succès</div>";
        $this->content .= "<div><a href='" . $this->router->showConnexion() . "'>Connectez-Vous</a> </div>";
        $this->content .= "</div>";

    }
}

?>
