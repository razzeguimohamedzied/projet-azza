<?php
require_once("view/View.php");
require_once("control/Controller.php");
require_once("model/UtilisateurStorage.php");
require_once("model/VoitureStorage.php");


class Router
{
    private $utilisateurStorage;
    private $voitureStorage;

    //contructeur router
    public function __construct(UtilisateurStorage $utilisateurStorage, VoitureStorage $voitureStorage)
    {
        $this->utilisateurStorage = $utilisateurStorage;
        $this->voitureStorage = $voitureStorage;
    }

    function main()
    {
        $view = new View($this);
        $controller = new Controller($view, $this->utilisateurStorage, $this->voitureStorage);

        $action = key_exists('action', $_GET) ? $_GET['action'] : null;

        if ($action === null) {
            $action = 'connexion';
        }

        switch ($action) {
            case "inscription":
                $controller->showInscriptionPage();
                break;

            case "save-inscription":
                $controller->saveNewInscription($_POST);
                break;

            case "connexion":
                $controller->showConnexion();
                break;

            case "form-voiture":
                $controller->showFormVoiture();
                break;

            case "save-voiture":
                $controller->saveVoiture($_POST);
                break;

            case "list-voiture":
                $controller->listVoiture();
                break;

            case "get-voiture":
                $controller->getVoiture($_GET['id']);
                break;

            case "update-voiture":
                $controller->updateVoiture($_GET['id']);
                break;

            case "update-voiture-form":
                $controller->updateVoitureForm($_GET['id'], $_POST);
                break;

            case "delete-voiture":
                $controller->deleteVoiture($_GET['id']);
                break;

            case "confirm-delete-voiture":
                $controller->confirmDeleteVoiture($_GET['id']);
                break;
        }
        $view->render();

    }

    public function saveInscription()
    {
        return "?action=save-inscription";
    }

    public function showInscriptionPage()
    {
        return "?action=inscription";
    }

    public function showConnexion()
    {
        return "?action=connexion";
    }

    public function listVoiture()
    {
        return "?action=list-voiture";
    }

    public function formVoiture()
    {
        return "?action=form-voiture";
    }

    public function saveVoiture()
    {
        return "?action=save-voiture";
    }

    public function showVoitureById($id)
    {
        return "?action=get-voiture&id=" . $id;
    }

    public function updateVoitureById($id)
    {
        return "?action=update-voiture&id=" . $id;
    }

    public function updateVoiture($id)
    {
        return "?action=update-voiture-form&id=" . $id;
    }

    public function deleteVoitureById($id)
    {
        return "?action=delete-voiture&id=" . $id;
    }

    public function confirmerSuppression($id)
    {
        return "?action=confirm-delete-voiture&id=" . $id;
    }
}

?>

