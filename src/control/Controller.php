<?php
require_once("view/View.php");
require_once("model/ConnexionBuilder.php");
require_once("model/UtilisateurBuilder.php");
require_once("model/VoitureBuilder.php");
require_once("model/Utilisateur.php");
require_once("model/UtilisateurStorage.php");
define('SITE_ROOT', realpath(dirname(__FILE__)));

class Controller
{

    protected $view;
    protected $utilisateurStorage;

//Ajouter l'interface UtilisateurStorage à la constructeur dans le but de pouvoir faire la liaison avec la BD
    public function __construct(View $view, UtilisateurStorage $utilisateurStorage, VoitureStorage $voitureStorage)
    {
        $this->view = $view;
        $this->utilisateurStorage = $utilisateurStorage;
        $this->voitureStorage = $voitureStorage;
    }

//form vide
    public function showInscriptionPage()
    {
        $builder = new UtilisateurBuilder();
        $this->view->showInscriptionPage($builder);
    }

//test form
    public function saveNewInscription($array)
    {
        $builder = new UtilisateurBuilder($array);

        if ($builder->isValid()) {
            // Save l'utilisateur dans la BD
            $utilisateur = $builder->createUtilisateur();
            $this->utilisateurStorage->create($utilisateur);
            $this->view->inscriptionSuccess();
        } else {
            $this->view->showInscriptionPage($builder);
        }
    }

    public function showConnexion()
    {
        $builder = new ConnexionBuilder();
        $this->view->showConnexionPage($builder);
    }

    public function showFormVoiture()
    {
        $builder = new VoitureBuilder();
        $this->view->showFormVoiture($builder);
    }

    public function saveVoiture($array)
    {
        $builder = new VoitureBuilder($array);

        if ($builder->isValid()) {

            $voiture = $builder->createVoiture();

            if (isset($_FILES["file"])) {
                $file_name = $_FILES["file"]["name"];
                $file_temp = $_FILES["file"]["tmp_name"];
                $folder = "uploads/";
                $moveFile = move_uploaded_file($file_temp, $folder . $file_name);

                if ($moveFile) {
                    $voiture->setImage($folder . $file_name);
                }
            }


            $this->voitureStorage->create($voiture);
            $this->view->ajoutVoitureSuccess();
        } else {
            $this->view->showFormVoiture($builder);
        }
    }

    public function listVoiture()
    {
        $voitures = $this->voitureStorage->readAll();
        $this->view->showListVoitures($voitures);
    }

    public function getVoiture($id)
    {
        $voiture = $this->voitureStorage->read($id);
        $this->view->getVoiture($voiture);
    }

    public function updateVoiture($id)
    {
        $voiture = $this->voitureStorage->read($id);
        $array = array();
        $array['categorie'] = $voiture->getCategorie();
        $array['marque'] = $voiture->getMarque();
        $array['modele'] = $voiture->getModele();
        $array['annee'] = $voiture->getAnnee();
        $builder = new VoitureBuilder($array);
        $this->view->showFormVoitureUpdate($builder, $id);
    }

    public function updateVoitureForm($id, $array)
    {
        $builder = new VoitureBuilder($array);

        if ($builder->isValid()) {
            // Save l'utilisateur dans la BD
            $voiture = $builder->createVoiture();
            if (isset($_FILES["file"])) {
                $file_name = $_FILES["file"]["name"];
                $file_temp = $_FILES["file"]["tmp_name"];
                $folder = "uploads/";
                $moveFile = move_uploaded_file($file_temp, $folder . $file_name);

                if ($moveFile) {
                    $voiture->setImage($folder . $file_name);
                }
            }

            $this->voitureStorage->update($id, $voiture);
            $this->view->updateVoitureSuccess();
        } else {
            $this->view->showFormVoitureUpdate($builder, $id);
        }
    }

    public function deleteVoiture($id)
    {
        $this->view->deleteVoiture($id);
    }

    public function confirmDeleteVoiture($id)
    {
        $this->voitureStorage->delete($id);
        $this->view->deleteVoitureSuccess();

    }
}

?>
