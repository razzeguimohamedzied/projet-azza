<?php
/*
 * On indique que les chemins des fichiers qu'on inclut
 * seront relatifs au répertoire src.
 */
set_include_path("./src");

/* Inclusion des classes utilisées dans ce fichier */
require_once("Router.php");
require_once("model/UtilisateurStorageDB.php");
require_once("model/VoitureStorageDB.php");
require_once("model/LoginDB.php");

/*
 * Cette page est simplement le point d'arrivée de l'internaute
 * sur notre site. On se contente de créer un routeur
 * et de lancer son main.
 */
//$router = new Router();
$loginBD = new LoginDB();
$router = new Router(new UtilisateurStorageDB($loginBD->getBD()), new VoitureStorageDB($loginBD->getBD()));
$router->main();
?>
